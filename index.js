
const FIRST_NAME = "Diana-Iuliana";
const LAST_NAME = "Balan";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
   var obj={};
   var cache={};
   cache.pageAccessCounter=function(name='home')
   {
       name=name.toLowerCase();
       if(obj[name]==undefined) obj[name]=1;
        else obj[name]++;
   }
   cache.getCache=function()
   {
       return obj;
   }
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

